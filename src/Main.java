import java.util.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Elevator elevator = new Elevator();
        System.out.println("Elevator is at floor " + elevator.getCurrentFloor());
        elevator.setDoorOpen(true);
        System.out.println("Door on floor " + elevator.getCurrentFloor() + " is open");
        Thread.sleep(1000);

        System.out.println("How many passengers ?");
        Scanner scanner = new Scanner(System.in);
        int users = scanner.nextInt();
        while(users <= 0 || users > 20){
            System.out.println("Please enter a valid number : between 1 and 20");
            scanner = new Scanner(System.in);
            users = scanner.nextInt();
        }

        int count = 0;
        while(count < users){
            elevator.getUsers().add(new User(elevator.getCurrentFloor()));
            count++;
        }
        elevator.getUsers().forEach(System.out::println);

        ArrayList<Integer> floors = new ArrayList<>();
        elevator.getUsers().forEach(User::callElevator);

        elevator.getUsers().forEach(System.out::println);

        for(int i=0;i<elevator.getUsers().size();i++){
            floors.add(elevator.getUsers().get(i).getDestinationFloor());
        }
        elevator.setUsersFloors(floors);

        System.out.println();
        while(elevator.getUsers().size() > 0){
            System.out.println("> Number of persons currently in the elevator : " + elevator.getUsers().size());
            int floorToGo = elevator.chooseShortestWay();
            if(floorToGo > elevator.getCurrentFloor()){
                elevator.goUp(floorToGo);
            } else {
                elevator.goDown(floorToGo);
            }
            elevator.goInOrOut();
            Thread.sleep(3000);
        }
        System.out.println("No one left in the elevator");
    }
}

/*Votre client vous propose de développer un software pour faire fonctionner des ascenseurs.
Les différents hardwares qui équiperont ces ascenseurs possèdent 2 commandes : goUp() et goDown().
Écrire dans le langage serveur de votre choix le software permettant de faire fonctionner
les ascenseurs équipés de ces hardwares. Votre client vous donne jusqu'à vendredi 14H
pour résoudre cette mission**/
