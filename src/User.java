import java.util.Scanner;

public class User{

    private static int ID = 0;
    private final int id;
    private int startingFloor, destinationFloor;
    private String direction, status;

    public User(int startingFloor){
        this.id = ++User.ID;
        this.startingFloor = startingFloor;
        this.destinationFloor = -1;
        this.direction = "";
        this.status = "outside";
    }

    public int getStartingFloor() { return startingFloor; }
    public int getDestinationFloor(){ return destinationFloor; }
    public void setDestinationFloor(int destinationFloor) { this.destinationFloor = destinationFloor; }

    public void callElevator(){
        if(status.equals("outside")){
            System.out.println("User " + id + " : Which floor do you want to go ?");
            Scanner floor = new Scanner(System.in);
            int floorToGo = floor.nextInt();

            while(!canGo(floorToGo) || !canMove(floorToGo, startingFloor)){
                System.out.println(!canGo(floorToGo) ?
                        "Please select a floor between 0 and 20"
                        : "You're already at floor " + floorToGo + " !");
                floor = new Scanner(System.in);
                floorToGo = floor.nextInt();
            }

            destinationFloor = floorToGo;
            if(floorToGo > startingFloor) direction = "up";
            else direction = "down";
            status = "inside";
        } else System.out.println("User " + id  + " is already inside the elevator !");
    }

    public void goInElevator(){
        if(canGo(destinationFloor) && canMove(destinationFloor, startingFloor) && status.equals("outside")){
            status = "inside";
            System.out.println("User " + id + " has entered the elevator");
            startingFloor = destinationFloor;
        } else System.out.println(!canGo(destinationFloor)
                ? "You can't go to that floor, it's outside limits" : (!canMove(destinationFloor, startingFloor))
                ? "You're already at floor " + destinationFloor + " !" : "You're already inside the elevator !");
    }

    public void goOutElevator(){
        if(status.equals("inside")){
            status = "outside";
            System.out.println("User " + id + " has left the elevator");
            startingFloor = destinationFloor;
        } else System.out.println("You're already outside the elevator !");
    }

    public boolean canMove(int destinationFloor, int startingFloor){
        return destinationFloor != startingFloor;
    }

    public boolean canGo(int floor){
        return floor >= 0 && floor <= 20;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", startingFloor=" + startingFloor +
                ", destinationFloor=" + destinationFloor +
                ", direction='" + direction + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
