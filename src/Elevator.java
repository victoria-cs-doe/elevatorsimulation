import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Elevator{

    private static final int maxFloor = 20;
    private int currentFloor;
    private ArrayList<Integer> usersFloors;
    private ArrayList<User> users;
    private boolean doorOpen;

    public Elevator(){
        this.currentFloor = ThreadLocalRandom.current().nextInt(0, maxFloor + 1);
        this.usersFloors = new ArrayList<>();
        this.users = new ArrayList<>();
        this.doorOpen = false;
    }

    public ArrayList<User> getUsers(){ return users; }
    public int getCurrentFloor() { return currentFloor; }
    public void setUsersFloors(ArrayList<Integer> usersFloors){ this.usersFloors = usersFloors; }
    public void setDoorOpen(boolean doorOpen) { this.doorOpen = doorOpen; }

    private boolean canGoUp(){ return currentFloor != maxFloor && !doorOpen; }
    private boolean canGoDown(){ return currentFloor != 0 && !doorOpen; }

    public void goInOrOut(){
        doorOpen = true;
        System.out.println("Door on floor " + currentFloor + " is open");
        ArrayList<User> usersToRemove = new ArrayList<>();

        for(User user : users){
            if(user.getStartingFloor() == currentFloor){
                user.goInElevator();
                usersFloors.add(user.getDestinationFloor());
            } else if(user.getDestinationFloor() == currentFloor){
                user.goOutElevator();
                usersToRemove.add(user);
            }
        }

        for(User user : usersToRemove){
            usersFloors.remove(Integer.valueOf(user.getDestinationFloor()));
            user.setDestinationFloor(-1);
            users.removeIf(user::equals);
        }
    }

    public void goUp(int floorToGo) throws InterruptedException {
        doorOpen = false;
        if(canGoUp()){
            System.out.println("Door on floor " + currentFloor + " is closed");
            System.out.println("Elevator going up...");
            countFloors(floorToGo);
            Thread.sleep(1000);
            currentFloor = floorToGo;
            System.out.println("Now at floor " + currentFloor);
        } else System.out.println("Can't go up, already at the highest level !");
    }

    public void goDown(int floorToGo) throws InterruptedException {
        doorOpen = false;
        if(canGoDown()){
            System.out.println("Door on floor " + currentFloor + " is closed");
            System.out.println("Elevator going down...");
            countFloors(floorToGo);
            Thread.sleep(1000);
            currentFloor = floorToGo;
            System.out.println("Now at floor " + currentFloor);
        } else System.out.println("Can't go down, already at the lowest level !");
    }

    public void countFloors(int floorToGo) throws InterruptedException {
        if(currentFloor < floorToGo){
            for(int i=currentFloor;i<floorToGo;i++){
                System.out.println("Floor " + (i+1) + "...");
                Thread.sleep(100);
            }
        } else {
            for(int i=currentFloor;i>floorToGo;i--){
                System.out.println("Floor " + (i-1) + "...");
                Thread.sleep(100);
            }
        }
    }

    public int chooseShortestWay(){
        Collections.sort(this.usersFloors);
        int shortestDistance = Math.abs(currentFloor - this.usersFloors.get(0));
        int floorToGo = usersFloors.get(0);
        for(int i : this.usersFloors){
            if(Math.abs(currentFloor - i) < shortestDistance){
                shortestDistance = Math.abs(currentFloor - i);
                floorToGo = i;
            }
        }
        return floorToGo;
    }

    @Override
    public String toString() {
        return "Elevator{" +
                "currentFloor=" + currentFloor +
                ", usersFloors=" + usersFloors +
                ", users=" + users +
                ", doorOpen=" + doorOpen +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Elevator elevator = (Elevator) o;
        return currentFloor == elevator.currentFloor
                && doorOpen == elevator.doorOpen
                && Objects.equals(usersFloors, elevator.usersFloors)
                && Objects.equals(users, elevator.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currentFloor, usersFloors, users, doorOpen);
    }
}


